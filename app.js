var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var lessMiddleware = require('less-middleware');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.disable('view cache');

app.engine('ejs', require('ejs-locals'));

//打开网页时弹出登录对话框
//如果不需要可以注销以下的代码
//==========================================================================================
app.use(function(req, res, next) {
  var auth = req.headers['authorization'];
  if(auth) {
    var tmp = auth.split(' ');
    var buf = new Buffer(tmp[1], 'base64');
    var plain_auth = buf.toString();
    var creds = plain_auth.split(':');
    var username = creds[0];
    var password = creds[1];
    if((username == 'admin') && (password == 'admin')) {
      //认证成功，允许访问
      return next();
    }
  }


  //要让浏览器弹出登录对话框，必须将status设为401，Header中设置WWW-Authenticate
  res.set('WWW-Authenticate', 'Basic realm=""');
  res.status(401).end();
});
//==========================================================================================
//打开网页时弹出登录对话框
//如果不需要可以注销以上的代码

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});



// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
