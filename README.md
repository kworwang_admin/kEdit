# kEdit

#### Description
一款基于nodejs实现的在线json编辑检测工具。

#### 基于以下技术

1.  node.js
2.  框架 express
3.  模版 ejs
4.  css支持 LESS
5.  开发工具 idea

#### 取消登陆验证
删除app.js中的以下代码
```
//打开网页时弹出登录对话框
//如果不需要可以注销以下的代码
//==========================================================================================
app.use(function(req, res, next) {
  var auth = req.headers['authorization'];
  if(auth) {
    var tmp = auth.split(' ');
    var buf = new Buffer(tmp[1], 'base64');
    var plain_auth = buf.toString();
    var creds = plain_auth.split(':');
    var username = creds[0];
    var password = creds[1];
    if((username == 'admin') && (password == 'admin')) {
      //认证成功，允许访问
      return next();
    }
  }


  //要让浏览器弹出登录对话框，必须将status设为401，Header中设置WWW-Authenticate
  res.set('WWW-Authenticate', 'Basic realm=""');
  res.status(401).end();
});
//==========================================================================================
//打开网页时弹出登录对话框
//如果不需要可以注销以上的代码
```

配置端口在`/bin/www`文件中。配置本地读取的json文件在`routes/index.js`中
启动
```
npm start
```


